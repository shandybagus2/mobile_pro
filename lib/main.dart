import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(const FungsiUtama());
}

class FungsiUtama extends StatelessWidget {
  const FungsiUtama({Key? key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("App Hello Shandy")
          ),
        body: Center(child: Text("Hello word"),
        )
      )
    );
  }
}